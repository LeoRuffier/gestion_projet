# Projet Licence Pro. Fortuna Ethereum

![](./Images/ethereum-logo.jpg)

### Pour qui est fait le projet ? 
Ce projet est fait pour toute personne voulant investir dans la Blockchain Ethereum par le biais d'une loterie
### Ce que souhaitent les utilisateurs de votre projet ?
Participer à un jeu d'argent avec les cryptomonnaies
### Qu'est ce que votre projet ?
C'est une lotterie (comme un loto par exemple) fonctionnant sur la Blockchain Ethereum
### Les fonctionnalités essentielles ?
Inscription à une lotterie en envoyant de l'Ether pour y participer
### Autres solutions concurrentes ?
PoolTogether, 22bet Casino
### Comment vous vous différenciez par rapport aux autres solutions ?
La mise en jeu requise est très basse.

# Prérequis :

## I - Outils web et portefeuille 

1) Il est recommandé d'utiliser ***Metamask*** pour ce projet 
2) Retenez bien votre clé privée (seed)
3) Ajouter le réseau de test Rinkeby :
* Nom du réseau : ***Rinkeby***
* URL RPC : https://rinkeby.infura.io/v3/
* Chain ID : ***4***
* Symbole de la devise : ***ETH***
* Bloc explorer URL : https://rinkeby.etherscan.io

4) Créer un compte sur infura.io ainsi qu'un nouveau projet.
5) Une fois celui-ci créer, cliquez dessus et sur "Project Settings" à droite de l'écran
6) Dans l'onglet ***"Keys"*** à coté de ***"Endpoint"*** sélectionnez ***"Rinkeby"***
7) Copier le lien commençant par ***"https"***

### Nous allons à présent ajouter un peu d'Ethereum à notre portefeuille pour mener à bien ce projet
8) Rendez-vous sur le site suivant : https://faucets.chain.link/
9) Connectez votre portefeuille ***Metamask*** au site.
10) Vérifier que ****"Network"*** soit bien égale à ***Ethereum Rinkeby***
11) Complétez le reCapta, après la validation de la TX, votre compte ***Metamask*** doit être créditer de ***0.1ETH***

## II - Installation des logiciels et des dépendances :

Pour déployer ce projet, nous allons avoir besoin de ***nodeJS*** :
1) Rendez-vous sur https://nodejs.org/en/download/
2) Sur votre éditeur de code, vous allez devoir installer le package pour la syntaxe ***Solidity***
3) Pour mener à bien certains tests de notre contrat, nous allons avoir besoin d'installer un module (sur un terminal): <br/>
```npm install solc@0.4.17```
4) Installez également la bibliothèque suivante : <br/>
```npm install mocha ganache-cli web3```
5) Celle-ci aussi : <br/>
```npm install @truffle/hdwallet-provider```

## III - Gérer les erreurs dans Visual Studio Code :

Il est possible que vous souhaitiez effectuer une modification sur notre smartcontract.
Notre contract repose sur la version 0.4.17 de Solidity. Pour éviter les bugs, il est recommandé que vous l'utilisiez aussi.

En Solidity, la version est indiquée à la première ligne du fichier ***.sol*** : <br/>
```pragma solidity ^0.4.17```

Votre éditeur de code peut vous générer une erreur : <br/>
![](./Images/v_error.png) <br/>

Pour remédier à cela, faite un clic-droit sur cette ligne et sélectionnez ***"Solidity: Change workspace compiler version (Remote)"*** et appliquez la version
du code : ***0.4.17***

## IV - Exemple simple du fichier Lottery.test.js :

Nous allons simplement vérifier ici que le contrat à bien été déployé lors de la phase de test : <br/>

```
const assert = require('assert');
const ganache = require('ganache-cli');
const Web3 = require('web3');
const web3 = new Web3(ganache.provider());

const { interface, bytecode } = require('../compile');

let lottery;
let accounts;

beforeEach(async () => {
    accounts = await web3.eth.getAccounts();
    lottery = await new web3.eth.Contract(JSON.parse(interface))
    .deploy({ data: bytecode})
    .send({ from: accounts[0], gas: '1000000' });
});

describe('Contrat loterie', () => {
    it('Déploiement du contrat', () => {
        assert.ok(lottery.options.address);
    });
});
```

Pour vérifier, ouvrez un terminal et rendez-vous ***dans*** le dossier de votre loterie et exécutez : <br/>
```
npm run test
```
Le résultat : <br/>
![](./Images/phase1_test.png)

# V - Création du site web à l'aide des packages React :

Allez dans un répertoire parent de votre PC et entrez la commande suivante : <br/>
```
npm install -g create-react-app
```
Pour appliquer ***React*** au projet, rendez-vous dans le ***répertoire où sont stockés vos fichiers de loterie*** et rentrez la commande suivante : <br/>
```
npx create-react-app loterie-react
```
Si vous obtenez des erreurs, rentrez la commande suivante : <br/>
```
npm uninstall -g create-react-app
```
Si vous êtes sur ***Linux/MAC-OS*** : <br/>
```
rm -rf /usr/local/bin/create-react-app
```
Si tout a bien fonctionné, vous devriez voir un nouveau fichier nommé ***'App.js'***, voici ce qu'il doit contenir : 
```
    import logo from "./logo.svg";
    import "./App.css";
    import React from "react";
     
    class App extends React.Component {
      render() {
        return (
          <div className="App">
            <header className="App-header">
              <img src={logo} className="App-logo" alt="logo" />
              <p>
                Edit <code>src/App.js</code> and save to reload.
              </p>
              <a
                className="App-link"
                href="https://reactjs.org"
                target="_blank"
                rel="noopener noreferrer"
              >
                Learn React
              </a>
            </header>
          </div>
        );
      }
    }
    export default App;
```
Après le déploiement de l'application REACT, il va falloir installer la bibliothèque web3 :
```
npm install web3
```

Une fois la bibliothèque installé, dans le dossier source, il va falloir créer le fichier ***web3***, voici son contenu : 
```
    import Web3 from "web3";

    window.ethereum.request({ method: "eth_requestAccounts" });
    const web3 = new Web3(window.ethereum);

    export default web3;
```
C'est ligne de code permettront à notre application JavaScript de demander l'accès à MetaMask.

Après le déploiement de votre application React, vous risquez d'avoir un message d'erreur à propos du versionning comme :
```
BREAKING CHANGE: webpack < 5 used to include polyfills for node.js core modules by default.
This is no longer the case. Verify if you need this module and configure a polyfill for it.
```
Pour corriger cela, nous allons devoir modifier des paramètres présents dans le fichier ***package.json*** <br/>
Rendez-vous à la ligne 11 où est indiquez :
```
"react-scripts": "5.0.1",
```
Et remplacez cette ligne par :
```
"react-scripts": "4.0.3",
```
Une fois cela fait, pensez à bien sauvegarder le fichier ***App.js*** puis dans un terminal, tapez les 3 commandes suivantes :
```
  rm -r node_modules
  rm package-lock.json
  npm install
```

Nous pouvons dès maintenant tester si notre application React fonctionne. Afin de vérifier, nous allons rajouter une ligne de code <br/>
juste en dessous de la fonction ***'App'*** :
```
  console.log(web3.version);
```
<br/>
Une fois cela fait, tapez dans votre terminal :
```
  npm run start
```
Cette ligne de commande permet de démarrer un serveur web sur votre adresse loopback et est directement accessible depuis un naviteur <br/>
sur http://localhost:3000/ . Evidemment, vous ne verrez rien directement, il faut faire un clic-droit, inspecter, console<br/>

A partir de là, vous devriez être un capacité de visualisé la version du package web3 installé : 

![](./Images/web3_ver.jpg)

# VI - Déploiement du smartcontrat : 

Nous allons à présent pouvoir déployer notre smartcontract. Avant cela, nous allons juste ajouter une ligne à notre fichier ***deploy.js*** <br/>
Au dessus de :
```
  console.log('Contract deployed to :', result.option.address);
```
Ajouter la ligne :
```
  console.log(interface);
```

Nous pouvons à présent entrer dans le terminal de notre contrat de loterie et tapez dans notre terminal : 
```
  node ./deploy.js
```

Après quelques secondes/minutes, le déploiement est normalement terminé. Vous pouvez voir une adresse, copiez là. <br/>
Nous allons pouvoir voir sur le site suivant https://rinkeby.etherscan.io/ le déploiement du contract, voici une exemple : 

![](./Images/Contrat.jpg)

# VII Implémentation dans notre application

Nous allons à présent faire en sorte que notre contrat de loterie soit relié à notre application React. <br/>
Pour cela, rendez-vous dans votre dossier contenant l'application React. <br/><br/>

Dans le dossier ***src/***, créez un nouveau fichier appelé ***loterie.js***. <br/>
Attention, il faut avoir copiez l'adresse à laquelle le contrat a été déployée ainsi que le contenue de l'ABI, voici le contenu du fichier :

```
  import web3 from "./web3";

  const address = 'ici votre adresse 0x...';
  const abi = contenue de l'abi : [{}]

  export default new web3.eth.Contract(abi, address);
```

Il va également falloir indiquer à notre fichier ***App.js*** où se situent les données (ABI + Adresse de déploiement) de notre contrat Ethereum. <br/>
Pensez-donc à ajouter au ***'import'*** d'ajouter le fichier ***loterie.js*** :
```
  import loterie from './loterie';
```

Nous allons à présent placer ici le fichier de ***App.js*** dit 'minimaliste' : 
```
import React from 'react'
import './App.css';
// Ajout du fichier de création d'instance pour MetaMask :
import web3 from './web3';
import loterie from './loterie';

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = { mananger: "" }
  }

  async componentDidMount() {
    const manager = await loterie.methods.manager().call();
    this.setState({ manager });
  }
  render() {
  return (
    <div>
      <h2>Lottery Contract</h2>
      <p>Ce contract est déployé par : {this.state.mananger}</p>
    </div>
  );
  }
}

export default App;
```

# Calcul des Sprints :
21/10 = 2,1 soit 2 Sprints
